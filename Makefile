all:	physix_xi.so plugin.py.test

#physix_py.so:	physix.py.o physix.o
#		g++ -g -ggdb -shared -o physix_py.so physix.py.o physix.o -lboost_python38 -lboost_numpy38 -L/usr/lib/x86_64-linux-gnu

physix_xi.so:	physix.xi.o physix.o
		g++ -g -ggdb -std=gnu++17 -shared -o physix_xi.so physix.xi.o physix.o -L/usr/lib/x86_64-linux-gnu

#physix.py.o:	src/physix.py.cc
#		g++ -g -ggdb -std=gnu++17 -fPIC -c src/physix.py.cc -I/usr/include/python3.8

physix.xi.o:	src/physix.xi.cc
		g++ -g -ggdb -std=gnu++17 -fPIC -c src/physix.xi.cc -I/usr/include/python3.8 -I./pybind11

physix.o:	src/physix.cc src/physix.hh
		g++ -g -ggdb -std=gnu++17 -fPIC -c src/physix.cc

plugin.py.test:	src/plugin.py.test.cc
		g++ -g -ggdb -std=gnu++17 -fPIC -o plugin.py.test src/plugin.py.test.cc  -I/usr/include/python3.8 -I./pybind11 -L/usr/lib/x86_64-linux-gnu -lpython3.8
