#include "physix.hh"

Object::Object(float x, float y) {
    this->x = x;
    this->y = y;
    this->visible = 0;
    v = glm::vec2(1.0, 0);
}

void Object::Set(std::tuple<float, float> v) {
    this->x = std::get<0>(v);
    this->y = std::get<1>(v);
}

std::tuple<float, float> Object::Get() {
    return {this->x, this->y};
}

void Object::status() {
    std::cout << "visible=" << this->visible << std::endl;
    std::cout << "v=" << this->v.x << ", " <<  this->v.y << std::endl;
    std::cout << "this=" << this << std::endl;
}

Object::~Object() {
}


Ball::Ball(float x, float y)
    : Object(x, y)
{
    len = 1.0;
}

void Ball::status() {
    Object::status();
    std::cout << "len=" << this->len << std::endl;
}

int Ball::test(Ball &b) {
    return -1;
}

int Ball::check(Ball &b) {
    std::cout << "this=" << this << std::endl;
    std::cout << "b=" << &b << std::endl;
    return len == b.len;
}

//int Ball::check(Ball *b) {
//    std::cout << "this=" << this << std::endl;
//    std::cout << "b=" << b << std::endl;
//    return len == b->len;
//}

Ball::~Ball() {
}

