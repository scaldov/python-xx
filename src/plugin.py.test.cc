#include <iostream>
#include <list>
#include <pybind11/pybind11.h>
#include <pybind11/embed.h>  // python interpreter
#include <pybind11/stl.h>  // type conversion
namespace py = pybind11;

int main(int argc, char** argv)
{
    py::scoped_interpreter guard{}; // start the interpreter and keep it alive
    py::module::import("sys").attr("path").attr("insert")(0, "./app");
    py::object plugin = py::module::import("plugin");
    py::function dir_recurse = py::reinterpret_borrow<py::function>(plugin.attr("dir_recurse"));
    py::object result = dir_recurse(std::string("."));
    const std::list<std::list<std::string> > &dirlist = result.cast<std::list<std::list<std::string> > >();
    for(auto l: dirlist){
        std::cout << "[" << std::endl;
        for(auto d: l){
            std::cout << d << std::endl;
        }
        std::cout << "]" << std::endl;
    }

    return 0;
}
