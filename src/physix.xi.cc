#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include "py.cast.hh"
#include "physix.hh"

namespace py = pybind11;

#include <iostream>
#include <string>
#include <tuple>

typedef double NPFLOAT;

class Object_py: public Object
{
public:
    Object_py(float x, float y)
        : Object(x, y)
    {}
    int _get_visible(){return visible;}
    void _set_visible(int visible){this->visible = visible;}
};


class Ball_py: public Object_py, public Ball
{
public:
    Ball_py(float x, float y)
        : Object_py(x, y), Ball(x, y)
    {}
};

PYBIND11_MODULE(physix_xi, m) {
    m.doc() = "documentation string"; // optional
    py::class_<Object_py>(m, "Object")
            .def(py::init<float, float>())
            .def_readwrite("x", &Object::x)
            .def_readwrite("y", &Object::y)
            .def_readwrite("v", &Object::v)
            .def("status", &Object::status)
            .def_property("visible", &Object_py::_get_visible, &Object_py::_set_visible);
    py::class_<Ball>(m, "_Ball");
    py::class_<Ball_py, Ball, Object_py >(m, "Ball")
            .def(py::init<float, float>())
            .def_readwrite("len", &Ball::len)
            //.def("check", [](Ball *self, Ball *b){return self->check(*b);}) // lambda variant
            .def("check", &Ball::check)
            .def("status", &Ball::status)
            ;
}
