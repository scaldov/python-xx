#include <iostream>
#include <string>
#include <tuple>

#include <glm/vec2.hpp>

class Object
{
public:
    glm::vec2 v;
    float x, y;
    int visible;
    Object(float x, float y);
    void Set(std::tuple<float, float> v);
    std::tuple<float, float> Get();
    void status();
    ~Object();
};

class Ball: public Object
{
public:
    float len;
    Ball(float x, float y);
    void status();
    int check(Ball &b);
    static int test(Ball &b);
//    int check(Ball *b);
    ~Ball();
};
