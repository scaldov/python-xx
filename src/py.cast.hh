#pragma once

#include <tuple>
#include <glm/vec2.hpp>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

namespace pybind11 { namespace detail {
template <> struct type_caster<glm::vec2 >
{
public:

    PYBIND11_TYPE_CASTER(glm::vec2, _("glm::vec2"));

    // conversion Python -> C++
    bool load(py::handle src, bool convert)
    {
        if (!convert && !py::array_t<float>::check_(src))
            return false;

        auto buf = py::array_t<float, py::array::c_style | py::array::forcecast>::ensure(src);
        if (!buf)
            return false;

        auto dims = buf.ndim();
        if (dims != 1 )
            return false;
        const float *vec = buf.data();
        value = glm::vec2(vec[0], vec[1]);
        return true;
    }

    //conversion C++ -> Python
    static py::handle cast(const glm::vec2& src, py::return_value_policy policy, py::handle parent)
    {
        py::array_t<float> a({1, 2}, (float*)&src );
        return a.release();
    }
};
}} // namespace pybind11::detail

