#include <boost/python.hpp>
#include <boost/python/numpy.hpp>

#include "physix.hh"

namespace bp = boost::python;
namespace np = boost::python::numpy;

#include <iostream>
#include <string>
#include <tuple>

typedef double NPFLOAT;

class Object_py: public Object
{
public:
    Object_py(float x, float y)
        : Object(x, y)
    {}
    float _get_x(){return x;}
    void _set_x(float x){this->x = x;}
    float _get_y(){return y;}
    void _set_y(float y){this->y = y;}
    int _get_visible(){return visible;}
    void _set_visible(int visible){this->visible = visible;}
    void _set_v(np::ndarray &v){
        NPFLOAT* vp = reinterpret_cast<NPFLOAT*>(v.get_data());
        this->v = glm::vec2(vp[0], vp[1]);
    }
    np::ndarray _get_v(){
        return np::from_data((float*)&v, np::dtype::get_builtin<float>(),
                             bp::make_tuple(2),
                             bp::make_tuple(sizeof(float)),
                             bp::object());
    }
};


class Ball_py: public Object_py, public Ball
{
public:
    Ball_py(float x, float y)
        : Object_py(x, y), Ball(x, y)
    {}
//    static float _get_len(Ball *self){return self->len;}
//    static void _set_len(Ball *self, float len){self->len = len;}
    float _get_len(){return len;}
    void _set_len(float len){this->len = len;}
};

BOOST_PYTHON_MODULE(physix_py)
{
    Py_Initialize();
    np::initialize();
//    bp::class_<Object_py>("Object")
//            .def("status", &Object::status)
//            .add_property("visible", &Object_py::_get_visible, &Object_py::_set_visible)
//            .add_property("x", &Object_py::_get_x, &Object_py::_set_x)
//            .add_property("y", &Object_py::_get_y, &Object_py::_set_y)
//            .add_property("v", &Object_py::_get_v, &Object_py::_set_v);
//    bp::class_<Ball_py, Ball_py*, bp::bases<Object_py> >("Ball", bp::init<>())
//            .def("status", &Ball::status)
//            .def("check", &Ball::check)
//            .add_property("len", &Ball_py::_get_len, &Ball_py::_set_len);
}
