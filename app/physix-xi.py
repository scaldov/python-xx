#!/usr/bin/python3

import os
import time
import json
import argparse
import platform
import random

parser = argparse.ArgumentParser()
#parser.add_argument('-t', '--timeout', help='timeout between attempts', required = False, default = 10)
#parser.add_argument('-a', '--auto', help='auto proceed to next attempt', required = False, action='store_true')
#parser.add_argument('-l', '--list', help='consonant list', required = False, default = consonants)
#parser.add_argument('-r', '--reverse', help='reverse dictation', required = False, action='store_true')
#parser.add_argument('-s', '--show', help='show full table', required = False, action='store_true')
#parser.add_argument('-k', '--katakana', help='train katakana', required = False, action='store_true')
#parser.add_argument('-c', '--cycle', help='one random cycle', required = False, action='store_true')
#args, unknown_args = parser.parse_known_args()

import numpy as np
from physix_xi import Ball, Object
o = Object(0.0, 0.0)
b = Ball(1.0, 1.5)

o.visible = 2
b.visible = 3
b.x = 4
b.y = 5
o.x = 6
o.y = 7
b.len = 8
print('obj:  ', o.status(), o.x, o.y)
print('ball: ', b.status(), b.x, b.y, b.len)
print(o.visible, b.visible)

o.v = np.array([3.,4.])
b.v = np.array([5.,6.])
print('obj v:  ', o.v)
print('ball v: ', b.v)
b1 = Ball(1.5, -1.0)
#b1.test()
b1.check(b)
