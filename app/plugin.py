#!/usr/bin/python3

import os
import time
import platform
import sys

def rdir(dir: str, path_list: list, level = 0):
    files_list = list()
    path_list.append(files_list)
    for p in os.listdir(dir):
        full_path = dir + '/' + p
        if (os.path.isdir(full_path)):
            rdir(full_path, path_list, level + 1)
        else:
            files_list.append(full_path)
    if level == 0:
        for p in path_list:
            if len(p) == 0:
                path_list.remove(p)


def dir_recurse(dir: str):
    paths = list()
    rdir(dir, paths)
    return paths


#paths = list()
#rdir('.', paths)
#for p in paths:
#    print(p)

