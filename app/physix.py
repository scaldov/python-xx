#!/usr/bin/python3

import os
import time
import json
import argparse
import platform
import random

parser = argparse.ArgumentParser()
#parser.add_argument('-t', '--timeout', help='timeout between attempts', required = False, default = 10)
#parser.add_argument('-a', '--auto', help='auto proceed to next attempt', required = False, action='store_true')
#parser.add_argument('-l', '--list', help='consonant list', required = False, default = consonants)
#parser.add_argument('-r', '--reverse', help='reverse dictation', required = False, action='store_true')
#parser.add_argument('-s', '--show', help='show full table', required = False, action='store_true')
#parser.add_argument('-k', '--katakana', help='train katakana', required = False, action='store_true')
#parser.add_argument('-c', '--cycle', help='one random cycle', required = False, action='store_true')
#args, unknown_args = parser.parse_known_args()

import numpy as np
import physix_py
o = physix_py.Object()
b = physix_py.Ball()
o.v = np.array([3.,4.])
b.v = np.array([5.,6.])
b1 = physix_py.Ball()
b1.check(b)
